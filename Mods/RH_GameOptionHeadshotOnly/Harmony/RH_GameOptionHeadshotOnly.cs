﻿using System;
using HarmonyLib;
using UnityEngine;
using System.Reflection;
using DMT;

public class RH_GameOptionHeadshotOnly
{
    public class RH_GameOptionHeadshotOnly_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityAlive))]
    [HarmonyPatch("DamageEntity")]
    [HarmonyPatch(new Type[] { typeof(DamageSource), typeof(int), typeof(bool), typeof(float) })]
    public class PatchEntityAliveDamageEntity
    {
        public static bool Prefix(EntityAlive __instance, ref DamageSource _damageSource, ref int _strength, bool _criticalHit, float _impulseScale)
        {
            if (RH_Options.GameOptions.HeadShotOnly && __instance is EntityZombie && !(__instance is EntityAnimalSnake))
            {
                if (_strength > 999)
                {                 
                    return true;
                }

                EnumBodyPartHit bodyPart = _damageSource.GetEntityDamageBodyPart(__instance);
                if (bodyPart == EnumBodyPartHit.Head)
                {
                       
                    _damageSource.DamageMultiplier = 1f;
                    _damageSource.DismemberChance += GetDismemberChance();
                }                  
                else
                {
                    _damageSource.DamageMultiplier = 0.00001f;
                    _strength = 0;
                }
            }

            return true;
        }

        private static float GetDismemberChance()
        {
            switch (GameStats.GetInt(EnumGameStats.GameDifficulty))
            {
                case 0:
                    return 0.2f;
                case 1:
                    return 0.16f;
                case 2:
                    return 0.12f;
                case 3:
                    return 0.08f;
                case 4:
                    return 0.04f;
                case 5:
                    return 0f;
            }

            return 0.8f;
        }
    }
}
