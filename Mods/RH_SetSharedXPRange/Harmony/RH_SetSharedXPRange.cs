﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_SetSharedXPRange
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(GameStats))]
    [HarmonyPatch("GetInt")]
    [HarmonyPatch(new Type[] { typeof(EnumGameStats) })]
    class PatchGameStatsGetIntEnum
    {
        static bool Prefix(GameStats __instance, ref int __result, ref EnumGameStats _eProperty)
        {
            if (_eProperty.Equals(EnumGameStats.PartySharedKillRange))
            {
                __result = 200;
                return false;
            }
            return true;
        }
    }

    [HarmonyPatch(typeof(GamePrefs))]
    [HarmonyPatch("GetInt")]
    [HarmonyPatch(new Type[] { typeof(EnumGamePrefs) })]
    class PatchGamePrefsGetIntEnum
    {
        static bool Prefix(GamePrefs __instance, ref int __result, ref EnumGamePrefs _eProperty)
        {
            if (_eProperty.Equals(EnumGamePrefs.PartySharedKillRange))
            {
                __result = 200;
                return false;
            }
            return true;
        }
    }
}
