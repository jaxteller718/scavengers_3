﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_WorkbenchCombineRepair
{
    public class Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_CombineGrid))]
    [HarmonyPatch("Merge_SlotChangedEvent")]
    [HarmonyPatch(new Type[] { typeof(int), typeof(ItemStack) })]
    class PatchXUiC_CombineGridMerge_SlotChangedEvent
    {
        static bool Prefix(XUiC_CombineGrid __instance, ref int slotNumber, ref ItemStack stack, ref XUiC_RequiredItemStack ___merge1, ref XUiC_RequiredItemStack ___merge2, ref XUiC_RequiredItemStack ___result1, ref ItemStack ___lastResult, ref float ___experienceFromLastResult)
        {
            if (___merge1.ItemStack.IsEmpty() || ___merge2.ItemStack.IsEmpty() || ___merge1.ItemStack.itemValue.type != ___merge2.ItemStack.itemValue.type || ___merge1.ItemStack.itemValue.UseTimes == ___merge1.ItemStack.itemValue.MaxUseTimes || ___merge2.ItemStack.itemValue.UseTimes == ___merge2.ItemStack.itemValue.MaxUseTimes)
            {
                // If this condition is true let the original code run and that will take care of the workflow.
                return true;
            }
            if (___merge1.ItemStack.itemValue.type == ___merge2.ItemStack.itemValue.type)
            {
                ItemStack itemStack1 = ___merge1.ItemStack;
                ItemStack itemStack2 = ___merge2.ItemStack;
                ItemStack itemStack3 = ___merge1.ItemStack.Clone();

                if (itemStack1.itemValue.Quality <= 40 && itemStack2.itemValue.Quality <= 40)
                    return false;

                bool hasMods = false;       
                foreach (var mod in itemStack1.itemValue.Modifications)
                {
                    if (mod != null && mod.IsMod)
                        hasMods = true;
                }
                foreach (var mod in itemStack2.itemValue.Modifications)
                {
                    if (mod != null && mod.IsMod)
                        hasMods = true;
                }
                if (hasMods)
                {
                    GameManager.ShowTooltipWithAlert(__instance.xui.playerUI.entityPlayer, Localization.Get("workbenchCombineHasMods"), "ui_denied");
                    return false;
                }


                float percent1 = (float)(itemStack1.itemValue.MaxUseTimes - itemStack1.itemValue.UseTimes) / (float)itemStack1.itemValue.MaxUseTimes;
                float percent2 = (float)(itemStack2.itemValue.MaxUseTimes - itemStack2.itemValue.UseTimes) / (float)itemStack2.itemValue.MaxUseTimes;
                float combinedPercent = Mathf.Min(percent1 + percent2, 1f);
                int newQuality = Mathf.Max(itemStack1.itemValue.Quality, itemStack2.itemValue.Quality) - 40;

                itemStack3.itemValue = new ItemValue(itemStack3.itemValue.type, newQuality, newQuality);
                itemStack3.itemValue.UseTimes = itemStack3.itemValue.MaxUseTimes - (int)(itemStack3.itemValue.MaxUseTimes * combinedPercent);

                ___result1.ItemStack = itemStack3;
                ___result1.HiddenLock = false;
                ___lastResult = itemStack3;
            }

            return false;
        }
    }
}
