using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;
using DMT;

public class RH_StashBackpack
{
    public class RH_StashBackpack_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_BackpackWindow))]
    [HarmonyPatch("BtnSort_OnPress")]
    [HarmonyPatch(new Type[] { typeof(XUiController), typeof(OnPressEventArgs) })]
    public class PatchXUiC_BackpackWindowBtnSort_OnPress
    {
        static bool Prefix(XUiC_BackpackWindow __instance, ref XUiController _sender, ref OnPressEventArgs _e)
        {
            ItemStack itemStack = null;
            if (__instance.xui.AssembleItem.CurrentItemStackController != null)
            {
                itemStack = __instance.xui.AssembleItem.CurrentItemStackController.ItemStack;
            }
            SortStacks(__instance.xui.PlayerInventory, __instance.xui);
            if (itemStack != null)
            {
                __instance.GetChildByType<XUiC_ItemStackGrid>().AssembleLockSingleStack(itemStack);
            }

            return false;
        }

        private static void SortStacks(XUiM_PlayerInventory __instance, XUi _xui)
        {
            ItemStack[] array = __instance.GetBackpackItemStacks();
            ItemStack[] arrayLockItems = new ItemStack[array.Length];
            var _XUiC_BackpackWindow = (XUiC_BackpackWindow)_xui.FindWindowGroupByName("backpack").GetChildById("windowBackpack");
            XUiC_ItemStackGrid childByType = _XUiC_BackpackWindow.GetChildByType<XUiC_ItemStackGrid>();
            var itemControllers = AccessTools.Field(typeof(XUiC_ItemStackGrid), "itemControllers").GetValue(childByType) as XUiController[];
            for (int i = 0; i <= array.Length - 1; i++)
            {
                // NOTE : The last item in array[] is not set within the outer loop, instead its set within the inner loops! This means we need to manually set the last item in our cloned array arrayLockItems
                // changed 'i < array.Length - 1' to 'i <= array.Length - 1' to load in the last item in the array but break out once arrayLockItems[i] is set

                var xuiC_ItemStack = (XUiC_ItemStack)itemControllers[i];
                if (xuiC_ItemStack.CustomAttributes.GetBool("IsDumpLocked"))
                {
                    arrayLockItems[i] = array[i];
                    array[i] = ItemStack.Empty.Clone();

                    if (i == array.Length - 1)
                        break;
                }
                else
                {
                    arrayLockItems[i] = ItemStack.Empty.Clone();
                    if (i == array.Length - 1)
                        break;

                    if (array[i].IsEmpty())
                    {
                        for (int j = i + 1; j < array.Length; j++)
                        {
                            var xuiC_ItemStack2 = (XUiC_ItemStack)itemControllers[j];
                            if (!array[j].IsEmpty() && !xuiC_ItemStack2.CustomAttributes.GetBool("IsDumpLocked"))
                            {
                                array[i] = array[j];
                                array[j] = ItemStack.Empty.Clone();
                                break;
                            }
                        }
                    }
                    if (!array[i].IsEmpty())
                    {
                        ItemClass itemClass = array[i].itemValue.ItemClass;
                        int num = itemClass.Stacknumber.Value - array[i].count;
                        if (!itemClass.HasQuality && num != 0)
                        {
                            for (int k = i + 1; k < array.Length; k++)
                            {
                                var xuiC_ItemStack2 = (XUiC_ItemStack)itemControllers[k];
                                if (xuiC_ItemStack2.CustomAttributes.GetBool("IsDumpLocked"))
                                    continue;

                                if (array[i].itemValue.type == array[k].itemValue.type)
                                {
                                    int num2 = Utils.FastMin(array[k].count, num);
                                    array[i].count += num2;
                                    array[k].count -= num2;
                                    num -= num2;
                                    if (array[k].count == 0)
                                    {
                                        array[k] = ItemStack.Empty.Clone();
                                    }
                                }
                                if (num == 0)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            array = StackSortUtil.SortStacks(array);
            int arrayId = 0;
            for (int i = 0; i <= arrayLockItems.Length - 1; i++)
            {
                if (arrayLockItems[i].IsEmpty())
                {
                    arrayLockItems[i] = array[arrayId];
                    arrayId++;
                }
            }

            var backpack = AccessTools.Field(typeof(XUiM_PlayerInventory), "backpack").GetValue(__instance) as Bag;
            backpack.SetSlots(arrayLockItems);
        }
    }

    [HarmonyPatch(typeof(XUiC_ItemStack))]
    [HarmonyPatch("Update")]
    [HarmonyPatch(new Type[] { typeof(float) })]
    public class PatchXUiC_ItemStackUpdate
    {
        static bool Prefix(XUiC_ItemStack __instance, ref float _dt, ref bool ___bLocked, ref bool ___isDragAndDrop, ref bool ___isOver)
        {
            LocalPlayerUI playerUI = __instance.xui.playerUI;
            PlayerActionsGUI guiactions = playerUI.playerInput.GUIActions;
            UICamera uiCamera = playerUI.uiCamera;

            if (__instance.WindowGroup.isShowing)
            {
                if (!___bLocked && !___isDragAndDrop)
                {
                    if (___isOver)
                    {
                        if (Input.GetKey(KeyCode.X) && (RH_Core.StashBackpackXMarkedTime == 0 || GameTimer.Instance.ticks > RH_Core.StashBackpackXMarkedTime))
                        {
                            ___isOver = false;
                            __instance.CustomAttributes.SetBool("IsDumpLocked", !__instance.CustomAttributes.GetBool("IsDumpLocked"));
                            UpdateBagLockedSlots(__instance.xui.PlayerInventory.GetBackpackItemStacks(), __instance.xui);
                            ___isOver = true;
                            RH_Core.StashBackpackXMarkedTime = GameTimer.Instance.ticks + 5;
                        }
                    }
                }
            }
         
            return true;
        }

        static void Postfix(XUiC_ItemStack __instance, ref float _dt, ref bool ___isOver)
        {
            if (__instance.WindowGroup.isShowing)
            {
                if (!___isOver && __instance.CustomAttributes.GetBool("IsDumpLocked"))
                    __instance.background.Color = new Color(0.960f, 0.513f, 0.478f);
            }
        }

        private static void UpdateBagLockedSlots(ItemStack[] backpackItemStacks, XUi _xui)
        {
            EntityPlayer entityPlayer = GameManager.Instance.World.GetPrimaryPlayer();
            var _XUiC_BackpackWindow = (XUiC_BackpackWindow)_xui.FindWindowGroupByName("backpack").GetChildById("windowBackpack");
            XUiC_ItemStackGrid childByType = _XUiC_BackpackWindow.GetChildByType<XUiC_ItemStackGrid>();
            var itemControllers = AccessTools.Field(typeof(XUiC_ItemStackGrid), "itemControllers").GetValue(childByType) as XUiController[];
            for (int i = 0; i < backpackItemStacks.Length; i++)
            {
                var xuiC_ItemStack = (XUiC_ItemStack)itemControllers[i];
                if (xuiC_ItemStack != null)
                    XUiC_RH_StashBackpack.BagLockedSlots[i] = xuiC_ItemStack.CustomAttributes.GetBool("IsDumpLocked");
            }
        }
    }

    [HarmonyPatch(typeof(PlayerDataFile))]
    [HarmonyPatch("Read")]
    [HarmonyPatch(new Type[] { typeof(PooledBinaryReader), typeof(uint) })]
    public class PatchPlayerDataFileRead
    {
        static void Postfix(PlayerDataFile __instance, PooledBinaryReader _br, uint _version)
        {
            XUiC_RH_StashBackpack.BagLockedSlots = RH_Core.LoadStashBackpack();
        }
    }

    [HarmonyPatch(typeof(PlayerDataFile))]
    [HarmonyPatch("Write")]
    [HarmonyPatch(new Type[] { typeof(PooledBinaryWriter) })]
    public class PatchPlayerDataFileWrite
    {
        static void Postfix(PlayerDataFile __instance, PooledBinaryWriter _bw)
        {
            RH_Core.SaveStashBackpack(XUiC_RH_StashBackpack.BagLockedSlots);
        }
    }
}
