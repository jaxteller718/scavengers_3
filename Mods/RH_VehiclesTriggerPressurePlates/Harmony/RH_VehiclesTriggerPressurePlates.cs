﻿using HarmonyLib;
using System.Reflection;
using UnityEngine;
using DMT;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;

public class RH_VehiclesTriggerPressurePlates
{
    public class RH_VehiclesTriggerPressurePlates_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(EntityVehicle))]
    [HarmonyPatch("PhysicsFixedUpdate")]
    public class PatchEntityVehiclePhysicsFixedUpdate
    {
        static void Postfix(EntityVehicle __instance)
        {
            var entityPlayer = __instance.GetAttachedPlayerLocal();
            if (entityPlayer != null)
            {
                __instance.world.CheckEntityCollisionWithBlocks(entityPlayer);
            }
        }
    }
}
