Key,Source,Context,English
ATBerryTeaDesc,items,Food,A warm healing tea made from blueberries and snowberries
ATMixedTeaDesc,items,Food,A cool refreshing mixed herbal tea that helps protect against the cold
ATIcedCoffeeDesc,items,Food,A cool energising iced coffee that helps protect against the cold whilst remaining just as hydrating as the hot counterpart
ATYuccaChipsDesc,items,Food,A handful of hearty wedge cut yucca chips
ATHashbrownDesc,items,Food,A small triangle of shredded fried potato. A nice snack
ATMashPotatoDesc,items,Food,This hefty pot of silky smooth mash potato could be a meal on it's own
ATCornGritsDesc,items,Food,A warm pot of boiled corn grits with animal fat
ATBlueberryJamDesc,items,Food,Sweet blueberry jam made from rendered fruits. Maybe too sweet on it's own
ATYuccaJamDesc,items,Food,Surprisingly savory whilst still being sweet. Yucca jam is made from rendered down yucca fruit
ATEggSaladDesc,items,Food,A basic egg salad made from boiled eggs. Boiled potato and oil dressing
ATFriedMeatDesc,items,Food,Meat that has been fried in a cornmeal coating
ATCornChipsDesc,items,Food,Crispy fried corn chips that make for a nice snack between bashing skulls in
ATBBJamSandwichDesc,items,Food,A simple cornbread sandwich with a sweet blueberry jam filling to help recover from a long day
ATYuccaJamSandwichDesc,items,Food,A simple cornbread sandwich with a savory yucca jam filling that helps get rid of even the strongest hunger pangs
ATCoffeeCakeDesc,items,Food,This cake offers the energising properties of coffee whilst still being delicious
ATMushroomPieDesc,items,Food,Savory and buttery pie crust filled to the brim with mushrooms
ATMeatAndBeerPieDesc,items,Food,Rich and savory pie crust with a meat and beer filling. Truly a feast fit for the best of the best 
ATFriedMushroomsDesc,items,Food,Whole mushrooms coated in cornmeal and fried. A very filling snack
ATFriedEggDesc,items,Food,A simple sunny side up fried egg. The main component of any good breakfast
ATFrenchToastDesc,items,Food,Egg soaked cornbread that has been fried. Simple yet tasty
ATHoneyGlazedMeatDesc,items,Food,Roasted meat with a deep honey glaze covering it
ATBlueberrySodaDesc,items,Food,A refreshing carbonated soda made from blueberries
ATCarbonatedWaterDesc,items,Food,Carbonated water that is both refreshing and useful for digestion
ATKvassSodaDesc,items,Food,Kvass soda made from fermented cornbread to make it carbonated. A very basic soda
ATVodkaDesc,items,Food,Fermented and distilled potato water that will improve your bartering. This stuff packs a punch
ATSnowberryGinDesc,items,Food,A dry gin made from snowberries that will boost your ability to break blocks
ATBlueberrySpongeDesc,items,Food,Fluffy sponge cake with a very sweet blueberry jam filling. Filling for both the body and soul.
ATYuccaSpongeDesc,items,Food,Sweet and fluffy sponge cake with a somewhat savory yucca jam filling. Extremely filling
ATCustardPieDesc,items,Food,Buttery pie crust filled to the brim with a dense custard.
ATEnergyTabletDesc,items,Food,Energy tablets are great for getting you out of sticky situations. However you can get addicted to them.
ATFudgeDesc,items,Food,Sugary fudge that melts in the mouth and rots away the teeth. You didn't need those molars anyway
ATPopcornDesc,items,Food,Popped corn - the movie-goers favourite. A basic snack between meals
ATRockCandyDesc,items,Food,Unflavoured candy made mostly from sugar. Will give you quite the sugar rush
ATRockCandyBlueberryDesc,items,Food,Blueberry flavoured rock candy that tastes good and will give you a sugar rush
ATRockCandySnowberryDesc,items,Food,Snowberry infused rock candy that will help with digestion and give you a sugar rush
ATSpongeCakeDesc,items,Food,Dense sponge cake with sugar icing atop it.
ATSugarExtractDesc,items,Food,You'd have to be pretty deperate to eat raw sugar. Used in soda, candies and cakes
ATSnowberryGinDrunkDesc,buffs,Buff,A strong taste. You are feeling destructive.
ATVodkaDrunkDesc,buffs,Buff,That stuff burns. You feel more talkative
ATSugarRushDesc,buffs,Buff,A slight rush from the amount of sugar you just ate. This allows you to slightly run faster
ATSugarRushName,buffs,Buff,Sugar Rush
ATSugarRushTooltip,buffs,Buff,That was a lot of sugar.
ATEnergyTabletDesc,buffs,Buff,You can run faster. reload slightly quicker and become less exhausted. 
ATEnergyTabletName,buffs,Buff,Energy Rush
ATEnergyTabletTooltip,buffs,Buff,What a rush!
ATColdDrinkName,buffs,Buff,Cold Drink
ATColdDrinkDesc,buffs,Buff,Iced drinks help you survive better in hot environments.