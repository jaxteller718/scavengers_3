﻿using DMT;
using GameSparks.Core;
using HarmonyLib;
using System;
using System.Reflection;
using UnityEngine;

public class RH_DisableGameSpark
{
    public class RH_DisableGameSpark_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(GameSparksManager))]
    [HarmonyPatch("PrepareAndSendRequest")]
    [HarmonyPatch(new Type[] { typeof(GSRequestData), typeof(string) })]
    public class PatchGameSparksManagerPrepareAndSendRequest
    {
        static bool Prefix(GameSparksManager __instance)
        {
            // Just dont run the original method!
            return false;
        }
    }
}