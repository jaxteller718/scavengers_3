﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;
using DMT;
using System.Reflection.Emit;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using InControl;

public class RH_10SlotToolbelt
{
    public static PlayerAction InventorySlot10 { get; set; }
    public static PlayerAction InventorySlot11 { get; set; }

    public class RH_10SlotToolbelt_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    private static byte NewToolBeltSize = 9;

    [HarmonyPatch(typeof(Inventory), MethodType.Constructor)]
    [HarmonyPatch("Inventory")]
    [HarmonyPatch(new Type[] { typeof(IGameManager), typeof(EntityAlive) })]
    public class PatchInventoryInventory
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_S)
                {
                    codes[i].opcode = OpCodes.Ldc_I4;
                    codes[i].operand = NewToolBeltSize + 2;
                }
            }

            codes = BumpToolBeltSize(codes, false);
            codes = BumpToolBeltSize(codes, true);

            return codes.AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(EntityVehicle.VehicleInventory))]
    [HarmonyPatch("SetupSlots")]
    public class PatchVehicleInventorySetupSlots
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = new List<CodeInstruction>(instructions);

            for (int i = 0; i < codes.Count; i++)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_S)
                {
                    codes[i].opcode = OpCodes.Ldc_I4;
                    codes[i].operand = NewToolBeltSize + 2;
                }
            }

            return codes.AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(Inventory), "CanStack", new Type[] { typeof(ItemStack) })]
    public class PatchInventoryCanStack
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(Inventory), "CanStackNoEmpty", new Type[] { typeof(ItemStack) })]
    public class PatchInventoryCanStackNoEmpty
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(ItemActionEntryEquip), "RefreshEnabled")]
    public class PatchItemActionEntryEquipRefreshEnabled
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(ItemActionEntryUse), "OnActivated")]
    public class PatchItemActionEntryUseOnActivated
    {
        static bool Prefix(ItemActionEntryUse __instance, ref ItemActionEntryUse.ConsumeType ___consumeType, ref int ___oldToolbeltFocusID)
        {
            if (__instance.ItemController.xui.isUsingItemActionEntryUse)
            {
                return false;
            }
            XUiC_ItemStack stackControl = (global::XUiC_ItemStack)__instance.ItemController;
            if (!stackControl.ItemStack.itemValue.ItemClass.CanExecuteAction(0, __instance.ItemController.xui.playerUI.entityPlayer, stackControl.ItemStack.itemValue) || !stackControl.ItemStack.itemValue.ItemClass.CanExecuteAction(1, __instance.ItemController.xui.playerUI.entityPlayer, stackControl.ItemStack.itemValue))
            {
                GameManager.ShowTooltipWithAlert(__instance.ItemController.xui.playerUI.entityPlayer, "You cannot use that at this time.", "ui_denied");
                return false;
            }
            __instance.ItemController.xui.isUsingItemActionEntryUse = true;
            ItemStack itemStack = new ItemStack(stackControl.ItemStack.itemValue.Clone(), 1);
            ItemStack itemStack2 = new ItemStack(stackControl.ItemStack.itemValue.Clone(), stackControl.ItemStack.count - 1);
            if (itemStack2.count == 0)
            {
                itemStack2 = ItemStack.Empty.Clone();
            }
            Inventory inventory = __instance.ItemController.xui.PlayerInventory.Toolbelt;
            if (___consumeType == ItemActionEntryUse.ConsumeType.Quest)
            {
                __instance.ItemController.xui.FindWindowGroupByName("questOffer").GetChildByType<XUiC_QuestOfferWindow>().ItemStackController = stackControl;
                stackControl.QuestLock = true;
            }
            else
            {
                stackControl.HiddenLock = true;
            }
            stackControl.WindowGroup.Controller.SetAllChildrenDirty();
            __instance.RefreshEnabled();
            ___oldToolbeltFocusID = inventory.GetFocusedItemIdx();
            int num = 0;
            if (stackControl.ItemStack.itemValue.ItemClass != null)
            {
                for (int i = 0; i < stackControl.ItemStack.itemValue.ItemClass.Actions.Length; i++)
                {
                    bool flag = false;
                    switch (___consumeType)
                    {
                        case ItemActionEntryUse.ConsumeType.Eat:
                        case ItemActionEntryUse.ConsumeType.Drink:
                        case ItemActionEntryUse.ConsumeType.Heal:
                            if (stackControl.ItemStack.itemValue.ItemClass.Actions[i] != null)
                            {
                                flag = true;
                            }
                            break;
                        case ItemActionEntryUse.ConsumeType.Read:
                            if (stackControl.ItemStack.itemValue.ItemClass.Actions[i] is ItemActionLearnRecipe)
                            {
                                flag = true;
                            }
                            break;
                        case ItemActionEntryUse.ConsumeType.Quest:
                            if (stackControl.ItemStack.itemValue.ItemClass.Actions[i] is ItemActionQuest)
                            {
                                flag = true;
                            }
                            break;
                        case ItemActionEntryUse.ConsumeType.Open:
                            if (stackControl.ItemStack.itemValue.ItemClass.Actions[i] is ItemActionOpenBundle)
                            {
                                flag = true;
                            }
                            break;
                    }
                    if (flag)
                    {
                        num = i;
                        break;
                    }
                }
            }
            if (___consumeType != ItemActionEntryUse.ConsumeType.Quest)
            {
                stackControl.ItemStack = itemStack2;
            }
            if (!itemStack.itemValue.ItemClass.Actions[num].UseAnimation && itemStack.itemValue.ItemClass.Actions[num].ExecuteInstantAction(__instance.ItemController.xui.playerUI.entityPlayer, itemStack, false, stackControl))
            {
                if (___consumeType != ItemActionEntryUse.ConsumeType.Quest)
                {
                    stackControl.HiddenLock = false;
                    stackControl.WindowGroup.Controller.SetAllChildrenDirty();
                }
                __instance.ItemController.xui.isUsingItemActionEntryUse = false;
                return false;
            }

            int oldToolbeltFocusID = ___oldToolbeltFocusID;
            GameManager.Instance.StartCoroutine(SimulateActionExecution(inventory, num, itemStack, delegate ()
            {
                stackControl.WindowGroup.Controller.SetAllChildrenDirty();
                inventory.SetHoldingItemIdx(oldToolbeltFocusID);
                inventory.SetItem(10, ItemStack.Empty.Clone());
                inventory.OnUpdate();
                MethodInfo method = AccessTools.Method(typeof(ItemActionEntryUse), "switchBack");
                GameManager.Instance.StartCoroutine(method.Invoke(__instance, new object[] { inventory }) as IEnumerator);
            }));

            return false;
        }
    }

    public static IEnumerator SimulateActionExecution(Inventory inventory, int _actionIdx, ItemStack _itemStack, Action onComplete)
    {
        inventory.SetItem(10, new ItemStack(_itemStack.itemValue.Clone(), 2));
        yield return new WaitForSeconds(0.1f);
        inventory.SetHoldingItemIdx(10);
        yield return new WaitForSeconds(0.1f);
        inventory.CallOnToolbeltChangedInternal();
        yield return new WaitForSeconds(0.1f);
        while (inventory.IsHolsterDelayActive())
        {
            yield return new WaitForSeconds(0.1f);
        }
        inventory.Execute(_actionIdx, false, null);
        yield return new WaitForSeconds(0.1f);
        inventory.Execute(_actionIdx, true, null);
        if (_itemStack.itemValue.ItemClass != null && _itemStack.itemValue.ItemClass.Actions.Length > _actionIdx && _itemStack.itemValue.ItemClass.Actions[_actionIdx] != null)
        {
            var slots = AccessTools.Field(typeof(Inventory), "slots").GetValue(inventory) as ItemInventoryData[];
            var entity = AccessTools.Field(typeof(Inventory), "entity").GetValue(inventory) as EntityAlive;
            slots[10].itemStack.itemValue.ItemClass.Actions[_actionIdx].OnHoldingUpdate(inventory.GetItemActionDataInSlot(10, _actionIdx));
            while (inventory.IsHoldingItemActionRunning())
            {
                yield return new WaitForSeconds(0.1f);
            }
            ItemStack itemStack = new ItemStack(_itemStack.itemValue.Clone(), 1);
            if (itemStack.itemValue.ItemClass.Actions[_actionIdx] is ItemActionEat)
            {
                if (((ItemActionEat)itemStack.itemValue.ItemClass.Actions[_actionIdx]).Consume)
                {
                    if (itemStack.count > 1)
                    {
                        itemStack.count--;
                    }
                    else if (!itemStack.itemValue.HasQuality || itemStack.itemValue.MaxUseTimes == 0)
                    {
                        itemStack = ItemStack.Empty.Clone();
                    }
                    else if (itemStack.itemValue.MaxUseTimes > 0)
                    {
                        itemStack.itemValue.UseTimes += EffectManager.GetValue(PassiveEffects.DegradationPerUse, itemStack.itemValue, 1f, entity, null, itemStack.itemValue.ItemClass.ItemTags, true, true, true, true, 1, true);
                    }
                }
                else if (entity as EntityPlayerLocal != null)
                {
                    (entity as EntityPlayerLocal).PlayerUI.xui.PlayerInventory.AddItem(itemStack, true);
                }
            }
        }
        while (inventory.IsHolsterDelayActive())
        {
            yield return new WaitForSeconds(0.1f);
        }
        onComplete();
        yield break;
    }

    [HarmonyPatch(typeof(ItemActionEntryUse), "OnDisabledActivate")]
    public class PatchItemActionEntryUseOnDisabledActivate
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }
    [HarmonyPatch(typeof(ItemActionEntryUse), "RefreshEnabled")]
    public class PatchItemActionEntryUseRefreshEnabled
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(XUiC_Toolbelt), "Update", new Type[] { typeof(float) })]
    public class PatchXUiC_ToolbeltUpdate
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(EntityVehicle), "hasGasCan", new Type[] { typeof(EntityAlive) })]
    public class PatchEntityVehiclehasGasCan
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(EntityAlive), "AttachToEntity", new Type[] { typeof(Entity), typeof(int) })]
    public class PatchEntityAliveAttachToEntity
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(PlayerMoveController), "Start")]
    public class PatchPlayerMoveControllerStart
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), true).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(PlayerMoveController), "Update")]
    public class PatchPlayerMoveControllerUpdate
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            var codes = BumpToolBeltSize(new List<CodeInstruction>(instructions), true);
            codes = BumpToolBeltSize(codes, false);
            return codes.AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(PlayerActionsLocal), "get_InventorySlotIsPressed")]
    public class PatchPlayerActionsLocalget_InventorySlotIsPressed
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(PlayerActionsLocal), "get_InventorySlotWasReleased")]
    public class PatchPlayerActionsLocalget_InventorySlotWasReleased
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(PlayerActionsLocal), "get_InventorySlotWasPressed")]
    public class PatchPlayerActionsLocalget_InventorySlotWasPressed
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }

        static bool Prefix(PlayerActionsLocal __instance, ref int __result, ref List<PlayerAction> ___InventoryActions)
        {
            if (___InventoryActions.Count == 9)
            {
                MethodInfo method = AccessTools.Method(typeof(PlayerActionSet), "CreatePlayerAction");

                InventorySlot10 = method.Invoke(__instance, new object[] { "Inventory10" }) as PlayerAction;
                InventorySlot10.UserData = new PlayerActionsBase.ActionUserData("inpActInventorySlot10Name", null, PlayerActionsBase.GroupToolbelt, PlayerActionsBase.EAppliesToInputType.None);

                InventorySlot11 = method.Invoke(__instance, new object[] { "Inventory11" }) as PlayerAction;
                InventorySlot11.UserData = new PlayerActionsBase.ActionUserData("inpActInventorySlot11Name", null, PlayerActionsBase.GroupToolbelt, PlayerActionsBase.EAppliesToInputType.None);


                InventorySlot10.AddDefaultBinding(Key.Key0);
                InventorySlot11.AddDefaultBinding(Key.Minus);

                ___InventoryActions.Add(InventorySlot10);
                ___InventoryActions.Add(InventorySlot11);
            }

            for (int i = 0; i < 10; i++)
            {
                if (___InventoryActions[i].WasPressed)
                {
                    __result = i;
                    return false;
                }
            }

            __result = -1;
            return false;
        }
    }

    [HarmonyPatch(typeof(EntityPlayerLocal), "dropBackpack", new Type[] { typeof(bool) })]
    public class PatchPlayerActionsLocaldropBackpack
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(ItemActionQuest), "ExecuteAction", new Type[] { typeof(ItemActionData), typeof(bool) })]
    public class PatchItemActionQuestExecuteAction
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    [HarmonyPatch(typeof(ItemActionQuest), "OnHoldingUpdate", new Type[] { typeof(ItemActionData) })]
    public class PatchItemActionQuestOnHoldingUpdate
    {
        static IEnumerable<CodeInstruction> Transpiler(MethodBase original, IEnumerable<CodeInstruction> instructions)
        {
            return BumpToolBeltSize(new List<CodeInstruction>(instructions), false).AsEnumerable();
        }
    }

    private static List<CodeInstruction> BumpToolBeltSize(List<CodeInstruction> codes, bool IsActualNumber)
    {
        for (int i = 0; i < codes.Count; i++)
        {
            if (IsActualNumber)
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_7)
                {
                    codes[i].opcode = OpCodes.Ldc_I4;
                    codes[i].operand = (int)NewToolBeltSize;
                }
            }
            else
            {
                if (codes[i].opcode == OpCodes.Ldc_I4_8)
                {
                    codes[i].opcode = OpCodes.Ldc_I4;
                    codes[i].operand = (int)NewToolBeltSize + 1;
                }

            }
        }

        return codes;
    }
}