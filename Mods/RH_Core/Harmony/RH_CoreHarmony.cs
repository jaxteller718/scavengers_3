﻿using DMT;
using HarmonyLib;
using System;
using System.Reflection;
using UnityEngine;

public class RH_CoreHarmony
{
    private const string RH_GamePrefSelectorId = "RH_GamePrefSelectorId";

    public class RH_CoreHarmony_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("Init")]
    [HarmonyPatch(new Type[] { })]
    public class PatchXUiC_GamePrefSelectorInit
    {
        static bool Prefix(XUiC_GamePrefSelector __instance)
        {
            if (__instance.ViewComponent.ID.StartsWith("RH_"))
            {
                __instance.CustomAttributes.SetString(RH_GamePrefSelectorId, __instance.ViewComponent.ID);

                var id = string.Empty;
                switch (__instance.ViewComponent.ID)
                {
                    case "RH_CityZombieMultiplier":
                        id = "UNUSED_OptionsImageEffects";
                        break;
                    case "RH_HeadShotOnly":
                        id = "UNUSED_OptionsFieldOfViewNew";
                        break;
                    case "RH_ShowFetchItemOnCompass":
                        id = "UNUSED_OptionsDeferredLighting";
                        break;
                    case "RH_ShowFetchDistanceIndicator":
                        id = "UNUSED_OptionsSSAO";
                        break;
                    case "RH_ZombieRage":
                        id = "UNUSED_OptionsReflectionCullList";
                        break;
                    case "RH_SightRange":
                        id = "UNUSED_OptionsReflectionFarClip";
                        break;
                    case "RH_WanderingHordeFrequency":
                        id = "UNUSED_OptionsReflectionShadowDistance";
                        break;
                    case "RH_WanderingHordeMultiplier":
                        id = "UNUSED_OptionsReflectionBounces";
                        break;
                }

                AccessTools.Field(typeof(XUiView), "id").SetValue(__instance.ViewComponent, id);
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("SetVisible")]
    [HarmonyPatch(new Type[] { typeof(bool) })]
    public class PatchXUiC_GamePrefSelectorSetVisible
    {
        static bool Prefix(XUiC_GamePrefSelector __instance, ref bool _visible)
        {
            if (IsRHGameOption(__instance))
            {
                __instance.ViewComponent.IsVisible = true;
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("ControlCombo_OnValueChanged")]
    [HarmonyPatch(new Type[] { typeof(XUiController), typeof(XUiC_GamePrefSelector.GameOptionValue), typeof(XUiC_GamePrefSelector.GameOptionValue) })]
    public class PatchXUiC_GamePrefSelectorControlCombo_OnValueChanged
    {
        static bool Prefix(XUiC_GamePrefSelector __instance, XUiController _sender, XUiC_GamePrefSelector.GameOptionValue _oldValue, XUiC_GamePrefSelector.GameOptionValue _newValue)
        {
            if (IsRHGameOption(__instance))
            {
                var id = __instance.CustomAttributes.GetString(RH_GamePrefSelectorId);

                switch (id)
                {
                    case "RH_CityZombieMultiplier":
                        RH_Options.GameOptions.CityZombieMultiplier = _newValue.Value;
                        break;
                    case "RH_HeadShotOnly":
                        RH_Options.GameOptions.HeadShotOnly = _newValue.Value == 1;
                        break;
                    case "RH_ShowFetchItemOnCompass":
                        RH_Options.GameOptions.ShowFetchItemOnCompass = _newValue.Value == 1;
                        break;
                    case "RH_ShowFetchDistanceIndicator":
                        RH_Options.GameOptions.ShowFetchDistanceIndicator = _newValue.Value == 1;
                        break;
                    case "RH_ZombieRage":
                        RH_Options.GameOptions.ZombieRage = _newValue.Value == 1;
                        break;
                    case "RH_SightRange":
                        RH_Options.GameOptions.SightRange = _newValue.Value;
                        break;
                    case "RH_WanderingHordeFrequency":
                        RH_Options.GameOptions.WanderingHordeFrequency = _newValue.Value;
                        break;
                    case "RH_WanderingHordeMultiplier":
                        RH_Options.GameOptions.WanderingHordeMultiplier = _newValue.Value;
                        break;
                }

                AccessTools.Method(typeof(XUiC_GamePrefSelector), "CheckDefaultValue").Invoke(__instance, new object[] { });
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("SetCurrentValue")]
    [HarmonyPatch(new Type[] { })]
    public class PatchXUiC_GamePrefSelectorSetCurrentValue
    {
        static bool Prefix(XUiC_GamePrefSelector __instance)
        {
            if (IsRHGameOption(__instance))
            {
                var id = __instance.CustomAttributes.GetString(RH_GamePrefSelectorId);
                var controlCombo = AccessTools.Field(typeof(XUiC_GamePrefSelector), "controlCombo").GetValue(__instance) as XUiC_ComboBoxList<XUiC_GamePrefSelector.GameOptionValue>;

                switch (id)
                {
                    case "RH_CityZombieMultiplier":
                        controlCombo.SelectedIndex = RH_Options.GameOptions.CityZombieMultiplier - 1;
                        break;
                    case "RH_HeadShotOnly":
                        controlCombo.SelectedIndex = (RH_Options.GameOptions.HeadShotOnly) ? 1 : 0;
                        break;
                    case "RH_ShowFetchItemOnCompass":
                        controlCombo.SelectedIndex = (RH_Options.GameOptions.ShowFetchItemOnCompass) ? 1 : 0;
                        break;
                    case "RH_ShowFetchDistanceIndicator":
                        controlCombo.SelectedIndex = (RH_Options.GameOptions.ShowFetchDistanceIndicator) ? 1 : 0;
                        break;
                    case "RH_ZombieRage":
                        controlCombo.SelectedIndex = (RH_Options.GameOptions.ZombieRage) ? 1 : 0;
                        break;
                    case "RH_SightRange":
                        controlCombo.SelectedIndex = RH_Options.ConvertSightRangeToIndex();
                        break;
                    case "RH_WanderingHordeFrequency":
                        controlCombo.SelectedIndex = RH_Options.GameOptions.WanderingHordeFrequency;   
                        break;
                    case "RH_WanderingHordeMultiplier":
                        controlCombo.SelectedIndex = RH_Options.GameOptions.WanderingHordeMultiplier - 1;
                        break;
                }

                AccessTools.Method(typeof(XUiC_GamePrefSelector), "CheckDefaultValue").Invoke(__instance, new object[] { });
                return false;
            }

            return true;
        }
    }

    [HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    [HarmonyPatch("IsDefaultValueForGameMode")]
    [HarmonyPatch(new Type[] { })]
    public class PatchXUiC_GamePrefSelectorIsDefaultValueForGameMode
    {
        static bool Prefix(XUiC_GamePrefSelector __instance, ref bool __result)
        {
            if (IsRHGameOption(__instance))
            {
                var controlCombo = AccessTools.Field(typeof(XUiC_GamePrefSelector), "controlCombo").GetValue(__instance) as XUiC_ComboBoxList<XUiC_GamePrefSelector.GameOptionValue>;
                var id = __instance.CustomAttributes.GetString(RH_GamePrefSelectorId);

                switch (id)
                {
                    case "RH_CityZombieMultiplier":
                        __result = controlCombo.Value.Value == RH_Options.GameOptions.CityZombieMultiplierDefault;
                        break;
                    case "RH_HeadShotOnly":
                        __result = (controlCombo.Value.Value == 1) == RH_Options.GameOptions.HeadShotOnlyDefault;
                        break;
                    case "RH_ShowFetchItemOnCompass":
                        __result = (controlCombo.Value.Value == 1) == RH_Options.GameOptions.ShowFetchItemOnCompassDefault;
                        break;
                    case "RH_ShowFetchDistanceIndicator":
                        __result = (controlCombo.Value.Value == 1) == RH_Options.GameOptions.ShowFetchItemOnCompassDefault;
                        break;
                    case "RH_ZombieRage":
                        __result = (controlCombo.Value.Value == 1) == RH_Options.GameOptions.ZombieRageDefault;
                        break;
                    case "RH_SightRange":
                        __result = controlCombo.Value.Value == RH_Options.GameOptions.SightRangeDefault;
                        break;
                    case "RH_WanderingHordeFrequency":
                        __result = controlCombo.Value.Value == RH_Options.GameOptions.WanderingHordeFrequencyDefault;
                        break;
                    case "RH_WanderingHordeMultiplier":
                        __result = controlCombo.Value.Value == RH_Options.GameOptions.WanderingHordeMultiplierDefault;
                        break;
                }

                return false;
            }

            return true;
        }
    }

    private static bool IsRHGameOption(XUiC_GamePrefSelector __instance)
    {
        var id = __instance.CustomAttributes.GetString(RH_GamePrefSelectorId);

        return (!string.IsNullOrEmpty(id) && id.StartsWith("RH_"));
    }

    [HarmonyPatch(typeof(XUiC_NewContinueGame))]
    [HarmonyPatch("SaveGameOptions")]
    [HarmonyPatch(new Type[] { })]
    public class PatchXUiC_NewContinueGameSaveGameOptions
    {
        static void Postfix(XUiC_NewContinueGame __instance)
        {
            Log.Out("SaveGameOptions");
            RH_Options.SaveGameOptions();
        }
    }

    //[HarmonyPatch(typeof(XUiC_GamePrefSelector))]
    //[HarmonyPatch("ControlText_OnChangeHandler")]
    //[HarmonyPatch(new Type[] { typeof(XUiController), typeof(string), typeof(bool) })]
    //public class PatchXUiC_GamePrefSelectorControlText_OnChangeHandler
    //{
    //    static bool Prefix(XUiC_GamePrefSelector __instance, XUiController _sender, string _text, bool _changeFromCode)
    //    {
    //        Log.Out("PatchXUiC_GamePrefSelectorControlText_OnChangeHandler - " + _text);

    //        return true;
    //    }
    //}
}

