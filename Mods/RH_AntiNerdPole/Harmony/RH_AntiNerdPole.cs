﻿using DMT;
using HarmonyLib;
using System;
using System.Reflection;
using UnityEngine;

public class RH_AntiNerdPole
{
    public class RH_AntiNerdPole_Init : IHarmony
    {
        public void Start()
        {
            Debug.Log(" Loading Patch : " + GetType().ToString());
            var harmony = new Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

    [HarmonyPatch(typeof(Block))]
    [HarmonyPatch("PlaceBlock")]
    [HarmonyPatch(new Type[] { typeof(WorldBase), typeof(BlockPlacement.Result), typeof(EntityAlive) })]
    public class PatchBlockPlaceBlock
    {
        static bool Prefix(Block __instance, ref global::WorldBase _world, ref global::BlockPlacement.Result _result, ref global::EntityAlive _ea)
        {
            // If you aren't on the ground, don't place the block.
            if ((__instance.GetBlockName().ToLower().Contains("woodframe") 
                || __instance.GetBlockName().ToLower().Contains("flagstoneframe") 
                || __instance.GetBlockName().ToLower().Contains("scrapironframe") 
                || __instance.GetBlockName().ToLower().Contains("rebarframe") 
                || __instance.GetBlockName().ToLower().Contains("haybale")) && (!_ea.onGround && !_ea.IsInElevator()))
                return false;

            return true;
        }
    }
}