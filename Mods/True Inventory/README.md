# True Inventory

**VERSION 2.0** - www.eihwaz.de - **CC BY-NC-ND-SA**

--- With the support of Mastermatrix, Telric and Khaine ---

[**Video Preview**](https://www.youtube.com/watch?v=X0K3HK6azng) (*click me*)

--- --- --- --- --- --- --- --- ---

**Installation via ModLauncher available!**

Manuell Installation:

1. Download and unpack the Zip data.

2. Place it into your "Mods" folder.

3. Have fun!

--- --- --- --- --- --- --- --- ---

**True Inventory Features**

This mod changes the existing inventory system and adds backpacks of different sizes. These can be produced in different ways and unlock up to 40 inventory slots. **All backpacks can be placed as a block.** 10 additional slots can be gained through different pocket mods. Steroids/Packmule have been changed and unlock another 10 slots (to rach the maximum).

* **4 Different Backpacks**

	-Shabby Backpack 	// 10 Slots 	// always unlocked							// Trader
	
	-Normal Backpack    // 30 Slots		// unlock via "Needle and Thread" Pockets	// Trader
	
	-Kucci Backpack		// 30 Slots		// unlock via schematic in loot				// Trader (Secret Stash 1)
	
	-Military Backpack	// 40 Slots		// unlock via "Needle and Thread" Complete	// Trader (Secret Stash 3)
	
* **1 Admin Backpack**

	-Gucci Backpack 		// 60 Slots 	// always unlocked in dev menue
	
	**All backpacks can be placed as a block.**
	
* **Pocket Mod Rework**

	-Small Pocket Mod	// 02 Slots		// always unlocked
	
	-Large Pocket Mod	// 04 Slots		// unlock via "Needle and Thread" Pockets
	
	-Coat Pocket  Mod	// 06 Slots		// unlock via "Needle and Thread" Complete
	
* **Steroid/Packmule Rework**

	-Steroids			// 10 Slots		// -
	
**Attention** - use drag and drop to equip backpacks to prevent visual bugs.

--- --- --- --- --- --- --- --- ---

**Copyrights: Attribution-NonCommercial-NoDerivatives 4.0 International**

You are allowed to use this mod for your private use (singleplayer/multiplayer/server).

You are allowed to use this mod as part of your custom modpack (as separated mod).

You are not allow to reupload it without permission!

You are not allow to charge money for it!

You are not allow to copy over hole parts of it for your own Mod and release it under your name.

Of cause your allow to take inspiration or copy single parts (**no assets**) to improve your existing code!

Its all about respect - just ask for it and do not steal it 👍